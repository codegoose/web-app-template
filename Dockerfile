from fedora:latest as builder
run yum update -y
run yum install nodejs yarnpkg -y
workdir /app
copy package.json .
run yarn install
copy src/ src/
copy static/ static/
copy postcss.config.cjs .
copy prettier.config.cjs .
copy svelte.config.js .
copy tailwind.config.cjs .
copy tsconfig.json .
copy vite.config.ts .
run yarn build

from fedora:latest
workdir /app
run yum update -y
run yum install nodejs -y
run yum install -y python310 && \
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python3.10 get-pip.py && \
    python3.10 -m pip install supervisor && \
    rm -f get-pip.py
copy --from=builder /app/build/ .
copy --from=builder /app/package.json .
copy supervisord.conf /etc/supervisor/conf.d/supervisord.conf
cmd ["/usr/local/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
